#!/bin/sh


git add .
git commit -m "🎉 ping service updated"

export APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))
export DOCKER_USER="registry.dev.test:5000"
export IMAGE_NAME="${APPLICATION_NAME}-img"
export TAG=$(git rev-parse --short HEAD)
export IMAGE="${DOCKER_USER}/${IMAGE_NAME}:${TAG}"

# build jar
./mvnw clean package

docker build -t ${IMAGE_NAME} .
docker tag ${IMAGE_NAME} ${IMAGE}
docker push ${IMAGE}

curl http://${DOCKER_USER}/v2/_catalog

export KUBECONFIG=../create-cluster/k3s.yaml

#only if you use nip.io or xip.io
export CLUSTER_IP=$(multipass info basestar | grep IPv4 | awk '{print $2}')

export NAMESPACE="training"

export CONTAINER_PORT=8080
export EXPOSED_PORT=80

export MQTT_CONTAINER_PORT=1883
export MQTT_EXPOSED_PORT=1883

export BRANCH=$(git symbolic-ref --short HEAD)

# Web domain
#only if you use nip.io or xip.io
#export HOST="${APPLICATION_NAME}.${BRANCH}.${CLUSTER_IP}.nip.io"
export DOMAIN="demo.k33g" # extension
export HOST="${APPLICATION_NAME}.${BRANCH}.${DOMAIN}"

envsubst < ./deploy.template.yaml > ./kube/deploy.${TAG}.yaml

kubectl apply -f ./kube/deploy.${TAG}.yaml -n ${NAMESPACE}

echo "🌍 http://${HOST}:${EXPOSED_PORT}"
echo "🌍 mqtt://${HOST}:${MQTT_EXPOSED_PORT}"
