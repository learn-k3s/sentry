package garden.bots.sentry

import io.netty.handler.codec.mqtt.MqttQoS
import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.Router
import io.vertx.ext.web.client.HttpResponse
import io.vertx.mqtt.MqttEndpoint
import io.vertx.mqtt.MqttServer.create
import io.vertx.mqtt.MqttServerOptions
import io.vertx.mqtt.MqttTopicSubscription
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj

class MainVerticle : AbstractVerticle() {

  override fun start(startPromise: Promise<Void>) {

    // internal ports
    val httpPort = System.getenv("CONTAINER_PORT")?.toInt() ?: 8080
    val mqttPort = System.getenv("MQTT_CONTAINER_PORT")?.toInt() ?: 1883
    //val host = System.getenv("HOST_IP") ?: "0.0.0.0"
    val host = "0.0.0.0"


    val options = MqttServerOptions()
    options.setPort(mqttPort)
    options.setHost(host)

    val mqttServer = create(vertx, options)
    val clientsMap : MutableMap<String, MqttEndpoint> =  mutableMapOf<String, MqttEndpoint>()
    val subscriptionsMap : MutableMap<String, MqttTopicSubscription> =  mutableMapOf<String, MqttTopicSubscription>()

    mqttServer.endpointHandler { endpoint ->

      // shows main connect info
      println("MQTT client [${endpoint.clientIdentifier()}] request to connect, clean session = ${endpoint.isCleanSession()}")

      // add or update client
      clientsMap.set(endpoint.clientIdentifier(), endpoint)

      // add or update subscription
      endpoint.subscribeHandler { mqttSubscribeMessage ->
        mqttSubscribeMessage.topicSubscriptions().forEach { subscription ->
          subscriptionsMap.set("${endpoint.clientIdentifier()}-${subscription.topicName()}", subscription)
          println("Subscription: ${endpoint.clientIdentifier()}  ${subscription.topicName()}")
        }
      }

      endpoint.publishHandler { mqttPublishMessage ->
        // you've got a message
        val topic = mqttPublishMessage.topicName()
        val message = Buffer.buffer(mqttPublishMessage.payload().toString())
        println("Topic: ${topic} Message: ${message}")

        clientsMap.forEach { clientEntry ->

          when (subscriptionsMap.get("${clientEntry.value.clientIdentifier()}-${topic}")) {
            null -> {
              // foo
            }
            else -> {
              clientEntry.value.publish(
                topic,
                message,
                MqttQoS.AT_LEAST_ONCE,
                false,
                false
              )
            }
          }
        }
      }
      // accept connection from the remote client
      endpoint.accept(false)
    }

    // --- http ---
    val router = Router.router(vertx)
    router.route().handler(io.vertx.ext.web.handler.BodyHandler.create())

    router.get("/api/hello").handler { context ->
      context.response().putHeader("content-type", "application/json;charset=UTF-8")
        .end(
          json { obj("message" to "👋 Hello World 🌍") }.encodePrettily()
        )
    }

    router.get("/").handler { context ->
      context.response().putHeader("content-type", "application/json;charset=UTF-8")
        .end(
          json { obj("message" to "👋 Hello World 🌍") }.encodePrettily()
        )
    }

    val httpServer = vertx.createHttpServer().requestHandler(router)

    mqttServer.listen { mqttAsyncResult ->
      when {
        mqttAsyncResult.failed() -> {
          println("😡 Error on starting the MQTT server")
          mqttAsyncResult.cause().printStackTrace()
          startPromise.fail(mqttAsyncResult.cause())
        }
        mqttAsyncResult.succeeded() -> {
          println("📡 MQTT server is listening on port ${mqttAsyncResult.result().actualPort()}")

          httpServer.listen(httpPort) { httpAsyncResult ->
            when {
              httpAsyncResult.failed() -> {
                println("😡 Error on starting the HTTP server")
                httpAsyncResult.cause().printStackTrace()
                startPromise.fail(httpAsyncResult.cause())
              }
              httpAsyncResult.succeeded() -> {
                startPromise.complete()
                println("🌍 HTTP server started on port ${httpPort}")
              }
            } // end of when
          } // end of httpServer.listen
        } // end of mqttAsyncResult.succeeded
      } // end of when
    } // end of httpServer.mqttServer
  } // end of start
} // end of MainVerticle
